<?php


use App\Models\Service;
namespace App\Models;

class SortingCenters extends Service{
    
    private $services;

    public function __construct($capacity){
        $this->capacity = $capacity;
    }

    public function addServices(array $services):void{
        $this->services = $services;
    }

    public function wasteTreatment(Waste $waste): float{
        $co2 = 0.0;
            if($waste->getKg() <= $this->capacity){
                $kg=$waste->getKg();
                foreach($this->services as $key => $service){
                    if($service->WasteAccept($waste) && !$service->isClose()){
                        $co2 += $service->WasteTreatment($waste);
                    }
                }
                $this->capacity -= $kg;
            }
        return $co2;
    }

    public function WasteAccept(Waste $waste) : bool{
        if( 'metaux' == $waste->getType())
            return true;
        return false;
    }

}