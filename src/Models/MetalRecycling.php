<?php

use App\Models\Service;
namespace App\Models;

class MetalRecycling extends Service {
    
    public function __construct($capacity){
        $this->capacity = $capacity;
    }

    public function wasteTreatment(Waste $waste): float{
        $co2= 0.0 ;
        if($this->capacity >= $waste->getKg()){
            $co2 = $waste->getKg()*7;
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());
        }else{
            $co2 = $this->capacity*7;
            $waste->removeKg($this->capacity);
            $this->capacity = 0;
        }
        return $co2;
    }

    public function WasteAccept(Waste $waste) : bool{
        if( 'metaux' == $waste->getType())
            return true;
        return false;
    }

}