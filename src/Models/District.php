<?php

namespace App\Models;

class District{

    /**
     * @var int
     */
    private $population;

    /**
     * @var Waste[]
     */
    private $wastes;

    public function __construct(int $population , array $wastes){
        $this->population = $population;
        $this->wastes = $wastes;
    }

    public function getWastes()  {
        return $this->wastes;
    }

    public function getPopulation() : int{
        return $this->population;
    }
    
}