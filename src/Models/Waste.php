<?php

namespace App\Models;

class Waste{

    /**
     * @var float
     */
    private $kg;
    /**
     * @var string
     */
    private $type;

    public function __construct(float $kg, string $type){
        $this->kg = $kg;
        $this->type = $type;
    }

    public function removeKg(float $kg){
        $this->kg -= $kg;
    }

    public function getKg(){
        return $this->kg;
    }

    public function getType(){
        return $this->type;
    }

    public function isTreated(){
        return $this->kg == 0;
    }
}