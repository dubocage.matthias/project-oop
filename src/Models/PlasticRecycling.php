<?php

use App\Models\Service;
namespace App\Models;

class PlasticRecycling extends Service{
    
    private $tabCo2 = ['PET' => 8, 'PVC' => 12 , 'PC' => 10, 'PEHD' => 11];
    private $tabPlasticAccept = [];

    public function __construct($capacity,$tab){
        $this->capacity = $capacity;
        $this->tabPlasticAccept = $tab;
    }

    public function wasteTreatment(Waste $waste): float{
        $co2= 0.0 ;
        if($this->capacity >= $waste->getKg()){
            $co2 = $waste->getKg()*$this->tabCo2[$waste->getType()];
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());
        }else{
            $co2 = $this->capacity*$this->tabCo2[$waste->getType()];
            $waste->removeKg($this->capacity);
            $this->capacity = 0;
        }
        return $co2;
    }

    public function WasteAccept(Waste $waste) : bool{
        foreach($this->tabPlasticAccept as $accept)
            if( $accept == $waste->getType())
                return true;
        return false;
    }
}