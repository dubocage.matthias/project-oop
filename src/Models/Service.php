<?php

use App\Models\Waste;
namespace App\Models;

abstract class Service{

    /**
     * @var float
     */
    protected $capacity;

    abstract protected function wasteTreatment(Waste $waste): float;

    public function isClose(){
        return $this->capacity == 0;
    } 

    public function getCapacity(){
        return $this->capacity;
    }

    abstract protected function WasteAccept(Waste $waste): bool;

}