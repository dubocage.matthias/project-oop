<?php

use App\Models\Service;
namespace App\Models;

class Incinerator extends Service{
    
    private $tabCo2 = ['PET' => 40 , 'PVC' => 38 , 'PC' => 42 , 'PEHD' => 35, 
     'organique' => 28, 'metaux' => 50 ,'verre' => 50,'papier' => 25, 'autre'=> 30];


    public function __construct($capacity){
        $this->capacity = $capacity;
    }

    public function wasteTreatment(Waste $waste): float{
        $co2= 0.0 ;

        if($this->capacity >= $waste->getKg()){
            $co2 = $waste->getKg()*$this->tabCo2[$waste->getType()];
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());
        }else{
            $co2 = $this->capacity*$this->tabCo2[$waste->getType()];
            $waste->removeKg($this->capacity);
            $this->capacity = 0;
        }

        return $co2;
    }

    public function WasteAccept(Waste $waste) : bool{
        return true;
    }
}