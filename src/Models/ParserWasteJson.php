<?php

use App\Models\District;
use App\Models\Waste;
use App\Models\Service;
use App\Models\Composter;
use App\Models\Interfaces\MoveWasteInterface;

use function PHPSTORM_META\type;

namespace App\Models;

class ParserWasteJson {

    /**
     * @var District[]
     */
    private $districts = [];

    /**
     * @var Service[]
     */
    private $services = [];

    /**
     * @var SortingCenters[]
     */
    private $sortingCenters = [];

    public function __construct(String $path){
        $this->districts = [];
        $jsondata = file_get_contents($path);
        $array = json_decode($jsondata);
        foreach($array->{'quartiers'} as $key1 => $quartier){
            $wastes = [];
            $population = 0;
            foreach($quartier as $key2 => $data){
                if($key2 == "population"){
                    $population = $data;
                }else{
                    if(is_object($data)){
                        foreach($data as $key3 => $data2){
                            array_push($wastes,new Waste($data2,$key3));
                        }   
                    }else{
                        array_push($wastes,new Waste($data,$key2));
                    }
                }
            }
            array_push($this->districts,new District($population , $wastes));
        }

        foreach($array->{'services'} as $key1 => $quartier){
            $service = "";
            $capacity = 1;
            $plastic = [];
            foreach($quartier as $key2 => $data){
                if($key2 == "type")
                    $service = $data;
                if($key2 == "ligneFour" || $key2 == "foyers")
                    $capacity *= $data;
                if($key2 == "capacite" || $key2 == "capaciteLigne")
                    $capacity *= $data;
                if($key2 == "plastiques")
                    $plastic = $data;
            }
            switch ($service) {
                case "composteur":
                    array_unshift($this->services , new Composter($capacity));
                    break;
                case "recyclagePapier":
                    array_unshift($this->services , new PaperRecycling($capacity));
                    break;
                case "recyclageMetaux":
                    array_unshift($this->services , new MetalRecycling($capacity));
                    break;
                case "recyclageVerre":
                    array_unshift($this->services , new GlassRecycling($capacity));
                    break;
                case "incinerateur":
                    array_push($this->services , new Incinerator($capacity));
                    break;
                case "recyclagePlastique":
                    array_unshift($this->services ,  new PlasticRecycling($capacity, $plastic));
                    break;
                case "centreTri":
                    array_push($this->sortingCenters , new SortingCenters($capacity));
                    break;
            }
        }
        

        foreach($this->services as $key => $service){
            echo  get_class($service) . " : " .$service->getCapacity() ."\n";
        }

        foreach($this->sortingCenters as $center){
            echo "Capacity : ". $center->getCapacity()." \n";
            $center->addServices($this->services);
            foreach($this->districts as $key => $district){
                if(!$center->isClose()){
                    echo "\n\nDistrict $key :\n";
                    $wastes = $district->getWastes();
                    foreach($wastes as $waste){
                        echo "Waste : ".$waste->getType(). ", KG :". $waste->getKg();
                        echo ", Co2 :". $center->wasteTreatment($waste);
                        echo ", KG  en sortie:". $waste->getKg(). "\n";
                    }
                }
            }
        }
        foreach($this->districts as $key => $district){
            echo "\n\nDistrict $key :\n";
            $wastes = $district->getWastes();
            foreach($wastes as $waste){
                echo $waste->getType(). ":". $waste->getKg();
                foreach($this->services as $service){
                    if($service->wasteAccept($waste) && !$service->isClose()){
                        echo ", Co2 :" .$service->wasteTreatment($waste);
                    }
                }
                echo "\n";
            }
        }
        foreach($this->districts as $key => $district){
            echo "\nDistrict $key :\n";
            $wastes = $district->getWastes();
            foreach($wastes as $waste){
                echo " ".$waste->getType(). ":". $waste->getKg().",";
            }
        }
        echo "\n\n";

        foreach($this->services as $key => $service){
            echo  get_class($service) . " : " .$service->getCapacity() ."\n";
        }
    }
}