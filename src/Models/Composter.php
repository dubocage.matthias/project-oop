<?php

use App\Models\Service;
namespace App\Models;

class Composter extends Service{

    public function __construct($capacity){
        $this->capacity = $capacity;
    }

    public function wasteTreatment(Waste $waste): float{
        $co2= 0.0 ;
        if($this->capacity >= $waste->getKg()){
            $co2 = $waste->getKg()*1;
            $this->capacity -= $waste->getKg();
            $waste->removeKg($waste->getKg());
        }else{
            $co2 = $this->capacity*1;
            $waste->removeKg($this->capacity);
            $this->capacity = 0;
        }
        return $co2;
    }

    public function WasteAccept(Waste $waste) : bool{
        if( 'organique' == $waste->getType())
            return true;
        return false;
    }
}